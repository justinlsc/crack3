#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <time.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

struct entry 
{
    char *hash;
    char *word;
};

int bcompare(const void *t, const void *a)
{
  return strcmp(t, *(char **)a);
  
}

int compare(const void *a, const void *b)
{
    return strcmp(*(char **)a, *(char **)b);
}

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Compare the two hashes
    if(strcmp(hash, guess) == 0) return 1;
    return 0;
}

int file_length(char *filename)
{
    struct stat fileinfo;
    if (stat(filename, &fileinfo) == -1)
        return -1;
    else
        return fileinfo.st_size;
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size, char **file)
{
    //Obtain length of file
    int len = file_length(filename);
    if (len == -1)
    {
        printf("Couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    //Allocate memory for the entire file
    char *file_contents = malloc(len);
    
    //Read entire file into file contents
    FILE *fp = fopen(filename, "r");
    if (!fp)
    {
        printf("Couldn't open %s for reading\n", filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    //Replace \n with \0.
    //Also keep count as we go.
    int line_count =0;
    for (int i = 0; i < len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    *size = line_count;
    struct entry *x;
    x = malloc(line_count * sizeof(struct entry));
    //Fill in each entry with address of corresponding line
    int c = 0;
    for (int i = 0; i < line_count; i++)
    {
        x[i].word=&file_contents[c];
        x[i].hash=md5(x[i].word, strlen(x[i].word));
        //Scan forward to find next line
        while (file_contents[c] != '\0') c++;
        c++;
    }
    
    *file = (file_contents);
    return x;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    clock_t begin = clock();
    
    printf("Generating rainbow table\n\n");
    
    int dlen;
    char *file;
    struct entry *dict = read_dictionary(argv[2], &dlen, &file);
    qsort(dict, dlen, sizeof(struct entry), compare);
    
    // Open the hash file for reading.
    //Read entire file into file contents
    FILE *fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Couldn't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    char line[PASS_LEN];
    int count =0;
    while(fgets(line, 100, fp) != NULL)
    {
        char *pos;
        if ((pos=strchr(line, '\n')) != NULL)
            *pos = '\0';
            
        struct entry *found = bsearch(line, dict, dlen, sizeof(struct entry), bcompare);
        //printf("%s\n", found);
        if (found != NULL)
        {
            count++;
            printf("%d) %s matches with %s\n",count, found->word,line); //causing errors in valgrind
        }
    }
    
    
    fclose(fp);
    for(int i = 0; i < dlen; i++ )
    {
        free(dict[i].hash);
    }
    
    free(dict);
    free(file);
    
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("\nExecution Time: %f\n", time_spent);
}
